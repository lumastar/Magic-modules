# EDWARDOtme magic modules

**This repository is now deprecated. Magic 2.0 has global variables which can replace the functionality of the TranslateShared and ScaleShared modules. Magic 2.11 has introduced an 'x for all' option in the Scale module, removing the need for a ScaleXYZ module. The latest tag should allow ScaleXYZ to be built for Magic 2.11 so that ScaleXYZ modules can be swapped for Scale modules in project files.**

This is a collection of custom OpenGL image processing modules developed for Magic, a music visualisation application. The modules are devoped using the Magic Module Devlopment Kit and written in C++ by EDWARDOtme.

For more information on Magic and the MDK see: https://magicmusicvisuals.com/ and https://magicmusicvisuals.com/developers