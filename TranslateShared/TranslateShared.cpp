// necessary for OpenGL
#ifdef _WIN32
#include <windows.h>
#include <gl/gl.h>
#pragma comment(lib, "OpenGL32.Lib")
#else // (assumes OS X)
#include <OpenGL/gl.h>
#include <stdlib.h>
#endif

#include <map>

#include "../MagicModule.h"

#define TranslateShared_NumParams 6

#define Param_Id 0
#define Param_Save 1
#define Param_X 2
#define Param_Link 3
#define Param_Y 4
#define Param_Z 5

struct TranslateDataset
{
	float x;
	float y;
	float z;

	bool link_xy;

	TranslateDataset(){
		x = 0;
		y = 0;
		z = 0;

		link_xy = false;
	}
};

class TranslateSharedModule : public MagicModule
{
	// see below (after class definition) for static value assignments
	static const MagicModuleSettings settings;
	static const MagicModuleParam params[TranslateShared_NumParams];
	static std::map<unsigned int, TranslateDataset*> dataset;

	TranslateDataset* myDataset;
	bool editEnabled;

public:

	TranslateSharedModule():MagicModule(){
		editEnabled = false;

		if(dataset.size() == 0){
			dataset[0] = new TranslateDataset();
		}
		myDataset = dataset[0];
	}
	
	void glInit(MagicUserData *userData) {
		editEnabled = userData->paramValues[Param_Save] > 0;

		if(editEnabled){
			unsigned int id = userData->paramValues[Param_Id];
			myDataset = dataset[id];
			if(myDataset == NULL){
				myDataset = new TranslateDataset();
				dataset[id] = myDataset;
			}
			
			myDataset->x = userData->paramValues[Param_X];
			myDataset->y = userData->paramValues[Param_Y];
			myDataset->z = userData->paramValues[Param_Z];
		}
	}

	const MagicModuleSettings *getSettings() { return &settings; }
	const MagicModuleParam *getParams() { return params; }

	void drawBefore(MagicUserData *userData) {
		if(userData->paramValues[Param_Save] > 0){
			myDataset->x = userData->paramValues[Param_X];
			myDataset->y = userData->paramValues[Param_Y];
			myDataset->z = userData->paramValues[Param_Z];
		}

		glPushMatrix();
		float y = myDataset->link_xy ? myDataset->x : myDataset->y;
		glTranslatef(myDataset->x, y, myDataset->z);
	};

	void drawAfter(MagicUserData *userData) {
		glPopMatrix();
	}
	
	bool fixedParamValueChanged(const int whichParam, const char* newValue) {
		if(!paramCurrentlyEnabled(whichParam))
			return false;

		if(whichParam == Param_Id){
			unsigned int value = atoi(newValue);

			TranslateDataset* v = dataset[value];
			if(v == NULL){
				v = new TranslateDataset();
				dataset[value] = v;
			}

			myDataset = v;

			return true;
		}

		if(whichParam == Param_Save){
			editEnabled = atoi(newValue);
			return true;
		}

		if(whichParam < Param_X)
			return true;

		if(whichParam == Param_X)
			myDataset->x = (float) atof(newValue);
		if(whichParam == Param_Y)
			myDataset->y = (float) atof(newValue);
		if(whichParam == Param_Z)
			myDataset->z = (float) atof(newValue);
		if(whichParam == Param_Link)
			myDataset->link_xy = atoi(newValue) > 0;

		return true;
	}

	bool paramCurrentlyEnabled(const int whichParam) {
		if(whichParam < Param_X)
			return true;

		if(whichParam == Param_Y && editEnabled)
			return !myDataset->link_xy;

		return editEnabled;
	}

	bool paramNeedsUpdating(const int whichParam) {
		return true; 
	}

	const char *getHelpText() {
		return "Shared Translate module.\n"
			"Note: save should only be enabled on one instance, and will save its visible values.\n\n"
			"Id:\nThe Id of the translate dataset.";
	}
};

MagicModule *CreateInstance() { return new TranslateSharedModule(); }

const MagicModuleSettings TranslateSharedModule::settings = MagicModuleSettings(TranslateShared_NumParams);
std::map<unsigned int, TranslateDataset*> TranslateSharedModule::dataset = std::map<unsigned int, TranslateDataset*>();

const MagicModuleParam TranslateSharedModule::params[TranslateShared_NumParams] = {
	MagicModuleParam("Id",     "0", "0",  NULL, MVT_INT,   MWT_TEXTBOX, false),
	MagicModuleParam("Save",   "0", NULL, NULL, MVT_BOOL,  MWT_TOGGLEBUTTON, false),
	MagicModuleParam("X",      "0", NULL, NULL, MVT_FLOAT, MWT_TEXTBOX, true),
	MagicModuleParam("Link XY",   "0", NULL, NULL, MVT_BOOL,  MWT_TOGGLEBUTTON, false),
	MagicModuleParam("Y",      "0", NULL, NULL, MVT_FLOAT, MWT_TEXTBOX, true),
	MagicModuleParam("Z",      "0", NULL, NULL, MVT_FLOAT, MWT_TEXTBOX, true)
};