Magic Module Development Kit (MDK) v1.6
---------------------------------------

The Magic MDK consists of a C++ header file (MagicModule.h) and example projects for XCode 4 and Visual Studio 2010 that can be used as a basis for creating new Magic modules.

Building the example projects will result in a dynamic library (.dll on Windows, .dylib on OS X) located in either the Debug or Release folder, depending on your current build configuration.  On Windows especially, make sure to build for the proper architecture (32- or 64-bit) depending upon your Magic version.

To have Magic recognize your new module, start the program and select "Help > Additional Module Folders".  Simply add your module's parent folder to the list.  After restarting, Magic will display the module in the "Add >" menu.


---

Magic MDK v1.6 - Copyright (c) 2012-2015 Color & Music, LLC.  All rights reserved.

By using the Magic MDK, you agree to the following terms:
The MDK is provided "as is" without any express or implied warranty of any kind, oral or written, including warranties of merchantability, fitness for any particular purpose, non-infringement, information accuracy, integration, interoperability, or quiet enjoyment.  In no event shall Color & Music, LLC or its suppliers be liable for any damages whatsoever (including, without limitation, damages for loss of profits, business interruption, loss of information, or physical damage to hardware or storage media) arising out of the use of, misuse of, or inability to use the MDK, your reliance on any content in the MDK, or from the modification, alteration or complete discontinuance of the MDK, even if Color & Music, LLC has been advised of the possibility of such damages.
