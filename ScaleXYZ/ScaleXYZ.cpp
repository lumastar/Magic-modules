// necessary for OpenGL
#ifdef _WIN32
#include <windows.h>
#include <gl/gl.h>
#pragma comment(lib, "OpenGL32.Lib")
#else // (assumes OS X)
#include <OpenGL/gl.h>
#include <stdlib.h>
#endif

#include "../MagicModule.h"

#define ScaleXYZ_NumParams 1

class ScaleXYZModule : public MagicModule
{
	// see below (after class definition) for static value assignments
	static const MagicModuleSettings settings;
	static const MagicModuleParam params[ScaleXYZ_NumParams];

public:

	const MagicModuleSettings *getSettings() { return &settings; }
	const MagicModuleParam *getParams() { return params; }

	void drawBefore(MagicUserData *userData) {
		glPushMatrix();
		float xyz = userData->paramValues[0];
		glScalef(xyz, xyz, xyz);
	};

	void drawAfter(MagicUserData *userData) {
		glPopMatrix();
	}

	const char *getHelpText() {
		return "ScaleXYZ module.\n"
			"Scale x, y and z by the same value.\n"
			"XYZ: The value by which to scale the x, y and z";
	}
};

MagicModule *CreateInstance() { return new ScaleXYZModule(); }

const MagicModuleSettings ScaleXYZModule::settings = MagicModuleSettings(ScaleXYZ_NumParams);

const MagicModuleParam ScaleXYZModule::params[ScaleXYZ_NumParams] = {
	MagicModuleParam("XYZ",      "1", NULL, NULL, MVT_FLOAT, MWT_TEXTBOX, true)
};